using Microsoft.AspNetCore.Mvc;
using SBApi.Models;
using SBApi.Services;

namespace SBApi.Controllers;


[ApiController]
[Route("[controller]")]
public class SBApiController : ControllerBase
{
    private readonly SBApiService _sBApiService;

    public SBApiController(SBApiService sBApiService)
    {
        _sBApiService = sBApiService;
    }

    [HttpPost]
    public async Task<IActionResult> CreateEquipmentContract(string productionRoomCode, string equipmentTypeCode, int equipmentCount)
    {
        var response = await _sBApiService.CreateEquipmentContract(productionRoomCode, equipmentTypeCode, equipmentCount);
        if (response.Status == ResultCode.Success)
        {
            return Ok(response.Data);
        }
        return BadRequest(response);
    }


    [HttpGet("/contracts")]
    public async Task<IActionResult> GetEquipmentContracts()
    {
        var response = await _sBApiService.GetEquipmentContracts();
        if (response.Status == ResultCode.Success)
        {
            return Ok(response.Data);
        }
        return BadRequest(response);
    }


}