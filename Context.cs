﻿using Microsoft.EntityFrameworkCore;
using SBApi.Models;

namespace SBApi
{
    public class Context : DbContext
    {

        public Context(DbContextOptions<Context> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<ProductionRoom> ProductionRooms { get; set; }
        public DbSet<EquipmentType> EquipmentTypes { get; set; }
        public DbSet<Contract> Contracts { get; set; }

    }
}
