﻿using System.Text.Json.Serialization;

namespace SBApi.Models
{
    public enum ResultCode
    {
        Success = 200,
        Error = 500,
        NotFound = 404
    }
    public class BaseResponse<T>
    {
        [JsonIgnore]
        public T Data { get; set; }
        public ResultCode Status { get; set; }
        public string ErrorMessage { get; set; }

        public BaseResponse()
        {
            Status = ResultCode.Success;
            ErrorMessage = "OK";
        }
    }
}
