﻿namespace SBApi.Models
{
    public class ProductionRoom
    {
        
    public int Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public double EquipmentAreaNorm { get; set; }
    
}
}
