﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;

namespace SBApi.Models
{
    public class Contract
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int ProductionRoomId { get; set; }
        [Required]
        public int EquipmentTypeId { get; set; }
        public int Quantity { get; set; }

        public ProductionRoom ProductionRoom { get; set; }
        public EquipmentType EquipmentType { get; set; }


    }
    

}
