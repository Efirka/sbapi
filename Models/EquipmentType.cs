﻿namespace SBApi.Models
{
    public class EquipmentType
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public double Area { get; set; }
    }
}
