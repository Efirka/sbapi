﻿
using Microsoft.EntityFrameworkCore;
using SBApi.Models;
using System.Collections.Generic;

namespace SBApi.Services
{
    public class SBApiService
    {
        Context _database;
        public SBApiService(Context database)
        {

            _database = database;
        }

        public async Task<BaseResponse<Contract>> CreateEquipmentContract(string productionRoomCode, string equipmentTypeCode, int equipmentCount)
        {
            BaseResponse<Contract> result = new BaseResponse<Contract>();

            try
            {
                using (Context db = _database)
                {
                    if (db.ProductionRooms.Count() == 0)
                    {
                        await InitDatabase(db);
                    }

                    if (string.IsNullOrWhiteSpace(productionRoomCode) || string.IsNullOrWhiteSpace(equipmentTypeCode) || equipmentCount <= 0)
                    {
                        throw new Exception("Invalid input data");
                    }

                    var productionRoom = db.ProductionRooms.FirstOrDefault(pr => pr.Code == productionRoomCode);
                    if (productionRoom == null)
                    {
                        throw new Exception($"Production room with code {productionRoomCode} not found");
                    }

                    var equipmentType = db.EquipmentTypes.FirstOrDefault(et => et.Code == equipmentTypeCode);
                    if (equipmentType == null)
                    {
                        throw new Exception($"Equipment type with code {equipmentTypeCode} not found");
                    }

                    double requiredArea = equipmentType.Area * equipmentCount;
                  

                    double availableArea = productionRoom.EquipmentAreaNorm - db.Contracts
                            .Include(c => c.EquipmentType)
                            .Where(c => c.ProductionRoomId == productionRoom.Id)
                            .Sum(c => c.Quantity * c.EquipmentType.Area);


                    if (requiredArea > availableArea)
                    {
                        throw new Exception("Not enough available space in production room");
                    }

                    var equipmentContract = new Contract
                    {
                        ProductionRoom = productionRoom,
                        EquipmentType = equipmentType,
                        Quantity = equipmentCount
                    };

                    db.Contracts.Add(equipmentContract);
                    db.SaveChanges();

                    result.Data = equipmentContract;
                }

            }
            catch (Exception ex)
            {
                result.Status = ResultCode.NotFound;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        public async Task<BaseResponse<IEnumerable<object>>> GetEquipmentContracts()
        {
            BaseResponse<IEnumerable<object>> result = new BaseResponse<IEnumerable<object>>();

            try
            {
                using (Context db = _database)
                {
                    if (db.ProductionRooms.Count() == 0)
                    {
                        await InitDatabase(db);
                    }

                    var query = from ec in db.Contracts
                                join pr in db.ProductionRooms on ec.ProductionRoomId equals pr.Id
                                join et in db.EquipmentTypes on ec.EquipmentTypeId equals et.Id
                                select new
                                {
                                    ProductionRoomName = pr.Name,
                                    EquipmentTypeName = et.Name,
                                    EquipmentCount = ec.Quantity
                                };

                    result.Data = await query.ToListAsync();
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultCode.NotFound;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        private async Task InitDatabase(Context db)
        {
            var firstForContract = new ProductionRoom()
            {
                Code = "MR12",
                Name = "Цех з виробництва м'яких меблів",
                EquipmentAreaNorm = 350
            };
            db.ProductionRooms.Add(firstForContract);

            db.ProductionRooms.Add(new ProductionRoom()
            {
                Code = "OR17",
                Name = "Цех з виробництва офісних меблів",
                EquipmentAreaNorm = 350
            });

            db.ProductionRooms.Add(new ProductionRoom()
            {
                Code = "KR24",
                Name = "Цех з виробництва корпусних меблів",
                EquipmentAreaNorm = 450
            });

            db.ProductionRooms.Add(new ProductionRoom()
            {
                Code = "DR28",
                Name = "Цех з виробництва меблів з цілісного дерева",
                EquipmentAreaNorm = 720.4
            });


            var secondForContract = new EquipmentType()
            {
                Code = "SE1",
                Name = "Стрічковопильні обладнання",
                Area = 20.4
            };

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "SE2",
                Name = "Сушильне устаткування",
                Area = 30.8
            });

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "DE1",
                Name = "Деревообробне обладнання",
                Area = 12.9
            });

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "SE3",
                Name = "Склообробне обладнання",
                Area = 17
            });

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "SE4",
                Name = "Швейне обладнання",
                Area = 8.6
            });

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "ME1",
                Name = "Металообробне обладнання",
                Area = 22.2
            });

            db.EquipmentTypes.Add(new EquipmentType()
            {
                Code = "DE2",
                Name = "Додатковий інструмент",
                Area = 4.5
            });


            db.Contracts.Add(new Contract()
            {
                ProductionRoom = firstForContract,
                EquipmentType = secondForContract,
                Quantity = 5
            });
            db.SaveChanges();
        }
    }
}

